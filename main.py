#!usr/bin/env python

##################################################################
# dnsec.py                                                       #
# description: Checks for DNS Misconfigurations/Vulnerability    #
# author: @UDP53                                                 #
# Should start writing now instead of ****                       #
##################################################################

import os
import socket
import requests
import time
import sys
import dns.query
import dns.zone
try:
	import dns.resolver 	
except ImportError:
	print "\n"+"do: pip install dnspython"

# will need much more dns related stuff Soon

class UDP53():
	def __init__(self):
		self.logo= '''

		DNS Security Analyzer Script - UDP53

				'''
		print self.logo
		if len( sys.argv) <2 :
			self.Usage()
		else:
			self.demo()

	def Usage(self):
		print ("[-] Usage: " + sys.argv[0] + " URL/IP")


	def demo(self):
		self.url=sys.argv[1]                                                 # Regex Check for URL 
		self.SameScripting()                                                 # IP to domain resolving
		self.ZoneTransfer()
		

	def SameScripting(self):
		print "[+] Testing for Same Site Scripting\n"                        # Test this on list of subdomains
		self.sameurl= 'localhost'+'.'+self.url
		try:
			self.sey=socket.gethostbyname(self.sameurl)
			if self.sey=='127.0.0.1':
				print "Vulnerable"+"\n"
			elif self.sey.find('.')!=-1:
				print "Not Vulnerable"+"\n"
		except socket.gaierror:     									     # Fucking Socket Exceptions 
			print "Cannot Resolve the Domain Name"+"\n"
			

	def ZoneTransfer(self):
		answers = dns.resolver.query(self.url,'NS')
		print "[-] Enumerating Targets Name Servers.....\n"
		catchzone=""
		for server in answers:
			self.ns = str(server)
			self.dns = dns.zone.from_xfr(dns.query.xfr(self.ns, self.url))   # AXFR Request
			zone = self.dns.nodes.keys()
			for n in zone:
				catchzone+= str(self.dns[n].to_text(n))
		print "[+] Attempting Zone Transfer with "+self.url+"'s"+" NameServers \n\n"                    
		print catchzone    				                                     # Write catchzone to output file    
			
sec=UDP53()


